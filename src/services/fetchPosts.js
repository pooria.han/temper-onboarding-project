import axios from 'axios';

export const fetchPosts = async () => {
  const response = await axios.get('https://jsonplaceholder.typicode.com/posts');

  if (response.status !== 200 || !response.data) {
    throw new Error('COULD_NOT_FETCH_POSTS');
  }

  if (!Array.isArray(response.data)) {
    throw new Error('POSTS_NOT_AN_ARRAY');
  }

  if (response.data.length < 2) {
    throw new Error('NOT_ENOUGH_POSTS');
  }

  return response.data;
}
