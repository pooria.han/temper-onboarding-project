import axios from 'axios';
import { fetchPosts } from './fetchPosts';

jest.mock('axios');

describe('fetchPosts()', () => {
  it('fetches posts successfully', async () => {
    const data = [{
        "userId": 1,
        "id": 1,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit",
      }, {
        "userId": 1,
        "id": 2,
        "title": "qui est esse",
        "body": "est rerum tempore",
      },];
    axios.get.mockResolvedValue({ status: 200, data });

    const result = await fetchPosts();
    expect(result).toEqual(data);
    expect(axios.get).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/posts');
  });

  it('throws an error when the response status is not 200', async () => {
    axios.get.mockResolvedValue({ status: 500, data: [] });

    await expect(fetchPosts()).rejects.toThrow('COULD_NOT_FETCH_POSTS');
  });

  it('throws an error when the response data is not an array', async () => {
    axios.get.mockResolvedValue({ status: 200, data: {} });

    await expect(fetchPosts()).rejects.toThrow('POSTS_NOT_AN_ARRAY');
  });

  it('throws an error when there are not enough posts', async () => {
    axios.get.mockResolvedValue({ status: 200, data: [{ id: 1 }] });

    await expect(fetchPosts()).rejects.toThrow('NOT_ENOUGH_POSTS');
  });
});
