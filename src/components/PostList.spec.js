import { shallowMount } from '@vue/test-utils';
import { createTestingPinia } from '@pinia/testing';
import PostList from '@/components/PostList.vue';
// import { useHistoryStore } from '@/stores/HistoryStore';
// jest.mock('@/stores/HistoryStore');

describe('PostList.vue', () => {
  let wrapper;
  
  beforeAll(() => {
    wrapper = shallowMount(PostList, {
      global: {
        plugins: [createTestingPinia()],
      }
    });
  });
  
  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });
});