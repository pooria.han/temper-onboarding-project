import { shallowMount } from '@vue/test-utils';
import Post from '@/components/Post.vue';

describe('Post.vue', () => {
  it('Component should be mounted', () => {
    const wrapper = shallowMount(Post, {
      props: {
        postId: 1,
        index: 0,
        length: 5,
      }
    });
    const firstElem = wrapper.find('.Post');
    expect(firstElem.exists()).toBe(true);
  })
});
