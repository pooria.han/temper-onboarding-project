import { setActivePinia, createPinia } from 'pinia';
import { useHistoryStore } from './HistoryStore';

describe('History Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });
  
  it('Add and Remove historyOfPostsOrder', () => {
    const store = useHistoryStore();
    expect(store.historyOfPostsOrder).toStrictEqual([]);
    
    store.addPostsOrder({uniqueId: 123, postId: 2});
    expect(store.historyOfPostsOrder).toStrictEqual([{uniqueId: 123, postId: 2}]);
    
    store.addPostsOrder({uniqueId: 123123, postId: 1});
    expect(store.historyOfPostsOrder).toStrictEqual([{uniqueId: 123, postId: 2}, {uniqueId: 123123, postId: 1}]);
    
    store.addPostsOrder({uniqueId: 123123123, postId: 22});
    store.addPostsOrder({uniqueId: 321, postId: 33});
    store.addPostsOrder({uniqueId: 321321, postId: 44});
    expect(store.historyOfPostsOrder).toStrictEqual(
      [{uniqueId: 123, postId: 2}, {uniqueId: 123123, postId: 1}, {uniqueId: 123123123, postId: 22}, {uniqueId: 321, postId: 33}, {uniqueId: 321321, postId: 44}]
      );
      
      store.removePostsOrder(1);
      expect(store.historyOfPostsOrder).toStrictEqual([{uniqueId: 123, postId: 2}, {uniqueId: 123123, postId: 1}, {uniqueId: 123123123, postId: 22}, {uniqueId: 321, postId: 33}]);
      
      store.removePostsOrder(2);
      expect(store.historyOfPostsOrder).toStrictEqual([{uniqueId: 123, postId: 2}, {uniqueId: 123123, postId: 1}]);
      
      store.removePostsOrder(2);
      expect(store.historyOfPostsOrder).toStrictEqual([]);
    });
    
    it('Add and Remove commits', () => {
      const store = useHistoryStore();
      expect(store.commits).toStrictEqual([]);
      
      // [{uniqueId, draggedPostId, oldIndex, newIndex}]
      store.addCommit({uniqueId: 123, draggedPostId: 2, oldIndex: 1, newIndex: 2});
      expect(store.commits).toStrictEqual([{uniqueId: 123, draggedPostId: 2, oldIndex: 1, newIndex: 2}]);
      
      store.addCommit({uniqueId: 123123, draggedPostId: 1, oldIndex: 2, newIndex: 1});
      expect(store.commits).toStrictEqual([{uniqueId: 123, draggedPostId: 2, oldIndex: 1, newIndex: 2}, {uniqueId: 123123, draggedPostId: 1, oldIndex: 2, newIndex: 1}]);
      
      store.addCommit({uniqueId: 123123123, draggedPostId: 22, oldIndex: 3, newIndex: 4});
      store.addCommit({uniqueId: 321, draggedPostId: 323, oldIndex: 1, newIndex: 4});
      store.addCommit({uniqueId: 321321, draggedPostId: 44, oldIndex: 0, newIndex: 4});
      expect(store.commits).toStrictEqual([
        {uniqueId: 123, draggedPostId: 2, oldIndex: 1, newIndex: 2},
        {uniqueId: 123123, draggedPostId: 1, oldIndex: 2, newIndex: 1},
        {uniqueId: 123123123, draggedPostId: 22, oldIndex: 3, newIndex: 4},
        {uniqueId: 321, draggedPostId: 323, oldIndex: 1, newIndex: 4},
        {uniqueId: 321321, draggedPostId: 44, oldIndex: 0, newIndex: 4}
      ]);
      
      store.removeCommits(1);
      expect(store.commits).toStrictEqual([
        {uniqueId: 123, draggedPostId: 2, oldIndex: 1, newIndex: 2},
        {uniqueId: 123123, draggedPostId: 1, oldIndex: 2, newIndex: 1},
        {uniqueId: 123123123, draggedPostId: 22, oldIndex: 3, newIndex: 4},
        {uniqueId: 321, draggedPostId: 323, oldIndex: 1, newIndex: 4}
      ]);

      store.removeCommits(4);
      expect(store.commits).toStrictEqual([]);
    });
  });
  