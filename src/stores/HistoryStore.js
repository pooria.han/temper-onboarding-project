import { defineStore } from 'pinia';

export const useHistoryStore = defineStore('HistoryStore', {
  state: () => ({
    commits: [],
    historyOfPostsOrder: [],
  }),
  actions: {
    /**
      * @param {array} latestPostsOrder
      * @param {number} latestPostsOrder.uniqueId - A unique id generated on the client side
      * @param {number} latestPostsOrder.postId - A post id returned from the API
    */
    addPostsOrder(latestPostsOrder) {
      this.historyOfPostsOrder.push(latestPostsOrder);
    },
    /**
      * @param {number} changes - The number of changes to revert
    */
    removePostsOrder(changes) {
      while (changes--) {
        this.historyOfPostsOrder.pop();
      }
    },
    /**
      * @param {object} commit
      * @param {number} commit.uniqueId - A unique id generated on the client side
      * @param {number} commit.draggedPostId - The selected post id
      * @param {number} commit.oldIndex - The old index of the post
      * @param {number} commit.newIndex - The new index of the post
    */
    addCommit(commit) {
      this.commits.push(commit);
    },
    /**
      * @param {number} changes - The number of changes to revert
    */
    removeCommits(changes) {
      while (changes--) {
        this.commits.pop();
      }
    },
    /**
      * @param {number} changes - The number of changes to revert
    */
    revertAction(changes) {
      this.removePostsOrder(changes);
      this.removeCommits(changes);
    }
  },
});
