import { mount, flushPromises, shallowMount } from '@vue/test-utils';
import { createTestingPinia } from '@pinia/testing';
import App from '@/App.vue';
import History from '@/components/History.vue';
import PostList from '@/components/PostList.vue';
// import { useHistoryStore } from '@/stores/HistoryStore';

// jest.mock('@/stores/HistoryStore');


describe('App.vue -- Success call', () => {
  let wrapper;
  
  beforeAll(() => {
    wrapper = mount(App, {
      global: {
        plugins: [createTestingPinia()],
      },
      stubs: {
        'History': History,
        'PostList': PostList
      }
    });
  });
  
  it('renders History and PostList components', () => {
    expect(wrapper.findComponent(History).exists()).toBe(true);
    expect(wrapper.findComponent(PostList).exists()).toBe(true);
  });
  
  it('rewindAnimation sets isRewinding to true and then false after 1 second', async () => {
    jest.useFakeTimers();
    wrapper.vm.rewindAnimation();
    expect(wrapper.vm.isRewinding).toBe(true);
    jest.advanceTimersByTime(1000);
    expect(wrapper.vm.isRewinding).toBe(false);
  });
});
