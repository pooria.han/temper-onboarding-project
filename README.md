# Temper Onboarding Project

## How to run the project

```
npm install
npm run serve
```

To run Unit tests:
```
npm run test:unit
```

_Note: the project runs on top of Node 18_


### Technical debts/Improvements
- Writing more Unit test cases
- ~~Writing JS Doc comments or moving to TypeScript~~ Added JS Doc
- Writing UI tests in case we need to render about 100 posts
